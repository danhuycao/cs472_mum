<%--
  Created by IntelliJ IDEA.
  User: Didi Oumar
  Date: 23/04/2018
  Time: 21:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Add User - Cycling Club</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <jsp:include page="/NavControl.jsp" flush="true"/>

    <div class="alert alert-success" id="divStatusMsg">
        <span id="spanStatusMessage"></span>
    </div>

<form action="UserServlet" method="post">

    <div style="margin-bottom: 20px ;">
        <c:forEach items="${error}" var="er">
        <span  class="alert alert-danger "> <c:out value="${er}" >  </c:out> </span>

        </c:forEach>
    </div>

        <div class="form-group">
            <label for="fname">First Name:</label>
            <input required type="text" class="form-control" id="fname" name="fname"
                   value="<c:out value='${cyclingevent.name}' />">
        </div>

        <div class="form-group">
        <label for="lname">last Name:</label>
        <input required type="text" class="form-control" id="lname" name="lname">
        </div>
         <div class="form-group">
        <label for="email">Email :</label>
        <input required type="text" class="form-control" id="email" name="email">
         </div>

    <div class="form-group">
        <label for="password"> Password :</label>
        <input required type="text" class="form-control" id="password" name="password">
    </div>



            <button type="submit" class="btn btn-primary" id="btnSaveUser">Submit</button>

    </form>
</form>
</div>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/myevents.js"></script>
</body>
</html>

