$(function(){

    /*$(document).on("click", ".deleteMyEvent", function(){
        var evtId = $(this).attr("data-id");
        var postData = { eventId: evtId };
        var thisRow = $(this).parent().parent();

        $.ajax({
            url: "/EventServlet",
            type: "post",
            data: postData,
            success: function(posts){
                $("#spanStatusMessage").html("Delete successful!");
                thisRow.remove();
            },
            error: function(xhr, code, exception){
                $("#spanStatusMessage").html(code + exception);
                ajaxFailure(xhr, code, exception);
            },
            complete: function(jqXHR, textStatus){
            }
        });
    });*/

    $(document).on("click", ".route", function(){

    });

    $("#btnSaveEvent").click(function(){
        var postData = {
            id: $("#eventId").val(),
            name: $("#eventname").val(),
            route: $("#route").val(),
            start_date: $("#start_date").val(),
            status: $("#selectStatus").val()
        };

        $.ajax({
            url: "/EventServlet?a=saveEvent",
            type: "post",
            data: postData,
            success: function(results){
                showMessage();
            },
            error: function(xhr, code, exception){
                logError(xhr, code, exception);
            },
            complete: function(jqXHR, textStatus){
            }
        });
    });

    hideMessage();
});

function showMessage(msg){
    $("#divStatusMsg").show();
    $("#divStatusMsg").html("Save successful!");
}
function hideMessage(msg){
    $("#divStatusMsg").hide();
    $("#divStatusMsg").html("");
}

function ajaxFailure(xhr, status, exception) {
    console.log(xhr, status, exception);
}