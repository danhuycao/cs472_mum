
$(function() {
    setInterval( myEmergency , 7000 );
    function myEmergency() {
        $.ajax({
            url: "http://localhost:8080/EmergencyEventServelet",
            type: "get",
            success: function (results) {
                if(isEmpty(results))
                {
                    $("#divResult").addClass("hide") ;
                }
                else
                {
                    $("#divResult").removeClass("hide") ;
                    console.log(results);
                    $("#divResult").html("");

                    /*
                    $("<tr>\n" +
                        "                    <th class=\"alert alert-danger \">Description</th>\n" +
                        "                    <th class=\"alert alert-danger \" >status</th>\n" +
                        "                </tr>").appendTo(table);*/
                    $(results).each(function (index) {
                        $("<div>" +
                            "   <span>" + this.description + "</span>\n" +
                            "&emsp;<span>" + this.status + "</span>" + "</div>").appendTo("#divResult");
                    });

                }},
            error: function (xhr, code, exception) {
                logError(xhr, code, exception);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    }


    function logError(xhr, code, exception){
        console.log(xhr + code + exception);
    }

    function isEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

});