<%--
  Created by IntelliJ IDEA.
  User: Amr Ibrahim
  Date: 4/22/2018
  Time: 2:57 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Title</title>
</head>
<body>

<table>
    <tr>
        <th>id</th>
        <th>name</th>
        <th>route</th>
        <th>start_date</th>
        <th>status</th>
        <th>owner_id</th>
        <th>emergency_flag</th>

    </tr>
    <c:forEach var="event" items="${events}">
        <tr>
            <td>${event.id}</td>
            <td>${event.name}</td>
            <td>${event.route}</td>
            <td>${event.start_date}</td>
            <td>${event.status}</td>
            <td>${event.owner_id}</td>
            <td>${event.emergency_flag}</td>
        </tr>
    </c:forEach></table>


</body>
</html>
