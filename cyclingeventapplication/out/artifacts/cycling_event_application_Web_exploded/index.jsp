<%--
  Created by IntelliJ IDEA.
  User: Didi Oumar
  Date: 22/04/2018
  Time: 15:50
  To change this template use File | Settings | File Templates.
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title> Ride Management</title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
  <script  src="js/index.js"></script>
  <link rel="stylesheet" href="css/style.css">
</head>

<body>

<div class="form">

<ul class="tab-group">

  <li class="tab active"><a href="#login">Log In</a></li>
  <li class="tab "><a href="#signup">Sign Up</a></li>

    <%--<li class="tab active"><a href="#signup">Sign Up</a></li>
    <li class="tab"><a href="#login">Log In</a></li>--%>

  </ul>

  <div class="tab-content">

    <div  id="login">
      <h1>Welcome Back!</h1>

      <form action="/login" method="post">

        <c:if test="${not empty msgUnvalid }">
          <div id="error" class="alert alert-danger ">${msgUnvalid}</div>
        </c:if>
        <div class="field-wrap">
          <label>
            Email Address<span class="req">*</span>
          </label>
          <input type="email"required name="email" autocomplete="off"/>
        </div>

        <div class="field-wrap">
          <label>
            Password<span class="req">*</span>
          </label>
          <input type="password" name="password"required autocomplete="off"/>
        </div>

        <p class="forgot"><a href="#">Forgot Password?</a></p>

        <button type="submit" id="button_login" class="button button-block"/>Log In</button>

      </form>

    </div>
    <div   id="signup">
      <h1>Please Sign up </h1>

      <form  action="/signup" method="post">

        <div class="top-row">
          <div class="field-wrap">
            <label>
              First Name<span class="req">*</span>
            </label>
            <input type="text" required autocomplete="off" name="first_name"/>
          </div>

          <div class="field-wrap">
            <label>
              Last Name<span class="req">*</span>
            </label>
            <input type="text"required autocomplete="off" name="last_name"/>
          </div>
        </div>

        <div class="field-wrap">
          <label>
            Email Address<span class="req">*</span>
          </label>
          <input type="email"required autocomplete="off" name="email"/>
        </div>

        <div class="field-wrap">
          <label>
            Set A Password<span class="req">*</span>
          </label>
          <input type="password"required autocomplete="off" name="password"/>
        </div>

        <button type="submit" class="button button-block"/> Get Started</button>

      </form>

    </div>

  </div><!-- tab-content -->

</div> <!-- /form -->








</body>

</html>
