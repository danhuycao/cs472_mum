package cs472.mum.enums;

public enum EventStatus {
    SCHEDULED,
    ACTIVE,
    PAUSE,
    EMERGENCY
}
