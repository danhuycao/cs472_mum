//// Didi
package cs472.mum.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User extends Model{

	//Fields
	private long userId;
	private String fname;
	private String lname;
	private String email;
	private String password;


	
	public User() {
		super();

	}

	public void setUserId(long userId){
		this.userId = userId;
	}

	public void setFname(String fname){
		this.fname = fname;
	}

	public void setLname(String lname){
		this.lname = lname;
	}

	public void setEmail(String email){
		this.email = email;
	}



	public void setPassword(String password){
		this.password = password;
	}



	public long getUserId(){
		return this.userId;
	}

	public String getFname(){
		return this.fname;
	}

	public String getLname(){
		return this.lname;
	}

	public String getEmail(){
		return this.email;
	}



	public String getPassword(){
		return this.password;
	}




	
	

}
