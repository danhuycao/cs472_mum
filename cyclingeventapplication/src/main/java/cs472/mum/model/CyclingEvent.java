package cs472.mum.model;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @version1.0
 * @author Amr Ibrahim
 *
 */


public class CyclingEvent {

    private int id;
    private String name;
    private String route;
    private LocalDate start_date;
    private String start_date_string;
    private String status;
    private long owner_id;
    private String emergency_flag;

    public CyclingEvent(int id, String name, String route, LocalDate start_date, String status, long owner_id, String emergency_flag) {
        this.id = id;
        this.name = name;
        this.route = route;
        this.start_date = start_date;
        this.status = status;
        this.owner_id = owner_id;
        this.emergency_flag = emergency_flag;

        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
        String date = DATE_FORMAT.format(Date.valueOf(start_date));
        this.start_date_string = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getStart_date_string() {
        return start_date_string;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public LocalDate getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDate start_date) {
        this.start_date = start_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(long owner_id) {
        this.owner_id = owner_id;
    }

    public String getEmergency_flag() {
        return emergency_flag;
    }

    public void setEmergency_flag(String emergency_flag) {
        this.emergency_flag = emergency_flag;
    }


    @Override
    public String toString() {
        return id + "," + name + "," + route;
    }
}
