package cs472.mum.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs472.mum.dao.UserDao;
import cs472.mum.model.User;
import cs472.mum.service.LoginService;


/**
 * 
 * 
 * @author Didi Oumar
 *
 */
@WebServlet("/login")
public class LoginCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private UserDao ud = new UserDao() ;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginCheckServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    System.out.println("doGet");
		request.getRequestDispatcher("myEvents.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost");
		String  email =request.getParameter("email");
		String password=request.getParameter("password");
	
		if (email .isEmpty()||password.isEmpty()) {
			request.setAttribute("msgUnvalid", "Wrong Username or Password");
		}
		LoginService logservice=new LoginService();

		User user = logservice.authorization(email, password);

		if (user==null) {


			request.setAttribute("msgUnvalid", "username or password are incorrect");
			request.setAttribute("tabindex" , "hide");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}else {
			   // UserDao ud = new UserDao();
			//request.getSession().setAttribute("user", user);
	      		 List<User> users = (List<User>)ud.findAll();
	      		 // chnage her
			     response.setContentType("application/json");
			     //end change
			request.getSession().setAttribute("MyUsers", users);
			request.getSession().setAttribute("currentuser" ,user ) ;

			response.sendRedirect("/ScheduledEventsServlet");
//			request.getRequestDispatcher("/WEB-INF/views/homepage.jsp").forward(request, response);
		}

	}

}
