package cs472.mum.controller;

import cs472.mum.model.EmergencyEvent;
import cs472.mum.service.EmergencyEventService;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

//@author Didi Oumar
@WebServlet(name = "/EmergencyEventServelet")
public class EmergencyEventServelet extends HttpServlet {

    private EmergencyEventService emerservic= new EmergencyEventService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

         List<EmergencyEvent> myEvents = this.emerservic.findAll() ;
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        List<JSONObject> listJSON = new ArrayList<>();
        System.out.println(myEvents.size());
         for (int i=0 ; i<myEvents.size() ; i++)
        {


                JSONObject jsonObject = new JSONObject();
                jsonObject.put("description",myEvents.get(i).getDescription());
                jsonObject.put("status",myEvents.get(i).getStatus());

                listJSON.add(jsonObject);
                System.out.println(jsonObject.toString());
        }
           out.print(listJSON.toString());
        // request.getSession().setAttribute("myEvents" , myEvents);
         //request.getRequestDispatcher("TestMyEvent.jsp").forward(request , response);
        out.flush();

    }
}
