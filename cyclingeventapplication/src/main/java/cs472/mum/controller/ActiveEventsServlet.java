package cs472.mum.controller;

import cs472.mum.model.User;
import cs472.mum.service.CyclingEventService;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;


/**
 *
 * @version1.0
 * @author Amr Ibrahim
 *
 */

@WebServlet("/ActiveEventsServlet")
public class ActiveEventsServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        CyclingEventService ds= new CyclingEventService();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        /*
        User u = new User();
        u.setUserId(1);
        u.setFname("amr");
        u.setLname("ibrahim");
        u.setPassword("123");
        u.setEmail("amr@mum.edu");
        request.getSession().setAttribute("user",u);
        */


        User u = (User)request.getSession().getAttribute("currentuser");
        System.out.println(u.getUserId() + "," + u.getFname()+ "," + u.getLname()+ "," + u.getEmail());


        JSONObject[] results = ds.getJSONCyclingEventByStatus("ACTIVE"); // Davenport
        System.out.println(Arrays.toString(results));
        out.print(Arrays.toString(results));
        out.flush();
    }
}
