package cs472.mum.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import cs472.mum.model.CyclingEvent;
import cs472.mum.service.*;


/**
 *
 * @version1.0
 * @author Amr Ibrahim
 *
 */

@WebServlet(name = "ScheduledEventsServlet")
public class ScheduledEventsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        CyclingEventService ses = new CyclingEventService();
        List<CyclingEvent> result =  ses.getCyclingEventByStatus("SCHEDULED");

        request.setAttribute("events", result);

        request.getRequestDispatcher("displayScheduledEvents.jsp").forward(request, response);
    }



}
