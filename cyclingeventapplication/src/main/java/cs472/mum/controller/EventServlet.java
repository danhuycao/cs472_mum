package cs472.mum.controller;

import cs472.mum.enums.EventStatus;
import cs472.mum.model.CyclingEvent;
import cs472.mum.model.User;
import cs472.mum.service.CyclingEventService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * EventServlet.java
 * This servlet acts as a Event controller for the application
 *
 * @author Dan Cao
 */

@WebServlet(name = "EventServlet")
public class EventServlet extends HttpServlet {

    private CyclingEventService sevice;

    public void init() {
        sevice = new CyclingEventService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("a");

        if ("addEvent".equalsIgnoreCase(action)) {
            showNewForm(request, response);
        } else if ("updateEvent".equalsIgnoreCase(action)) {
            showEditForm(request, response);
        } else if ("saveEvent".equalsIgnoreCase(action)) {
            try {
                insertEvent(request, response);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else if ("modifyEvent".equalsIgnoreCase(action)) {
            try {
                updateEvent(request, response);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            showMyEvents(request, response);
        }
    }

    private CyclingEvent getCyclingEventFromRequest(HttpServletRequest request) {
        int id = request.getParameter("id") == null ? -1 : Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String route = request.getParameter("route");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate start_date = LocalDate.parse(request.getParameter("start_date"), formatter);
        String status = request.getParameter("status").toUpperCase();
        String emergency_flag = request.getParameter("emergency_flag").toUpperCase();
        long owner_id = getLoggedUserId(request);

        if(!status.equalsIgnoreCase(EventStatus.EMERGENCY.toString())) emergency_flag = "";

        return new CyclingEvent(id, name, route, start_date, status, owner_id, emergency_flag);
    }

    private long getLoggedUserId(HttpServletRequest request){
        User loggedUser = (User)request.getSession().getAttribute("currentuser");
        return loggedUser.getUserId();
    }

    private void insertEvent(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        CyclingEvent ce = getCyclingEventFromRequest(request);
        Boolean b = sevice.insertCyclingEvent(ce);

        PrintWriter out = response.getWriter();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        out.print(b ? "true" : "false");
        out.flush();
    }
    private void updateEvent(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        CyclingEvent ce = getCyclingEventFromRequest(request);
        Boolean b = sevice.updateCyclingEvent(ce);

        PrintWriter out = response.getWriter();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        out.print(b ? "true" : "false");
        out.flush();
    }

    private void showMyEvents(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        long owner_id = getLoggedUserId(request);
        List<CyclingEvent> lstCe = sevice.getCyclingEventByUserId(owner_id);
        request.setAttribute("MyEvents", lstCe);

        request.getRequestDispatcher("myEvents.jsp").forward(request, response);
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("saveEvent.jsp").forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        CyclingEvent cyclingevent = sevice.getCyclingEventById(id);

        request.setAttribute("cyclingevent", cyclingevent);
        request.getRequestDispatcher("saveEvent.jsp").forward(request, response);

    }
}
