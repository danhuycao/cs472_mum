package cs472.mum.controller;
// Didi Oumar

import cs472.mum.model.User;
import cs472.mum.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "/UserServlet")
public class UserServlet extends HttpServlet {
    private UserService userService = new UserService();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String fname = request.getParameter("fname") ;
        String lname = request.getParameter("lname") ;
        String email = request.getParameter("email") ;
        String password = request.getParameter("password") ;
        User u = new User() ;
        u.setFname(fname);
        u.setLname(lname);
        u.setEmail(email);
        u.setPassword(password);
        List<String> error = userService.validateUser(u);
        if(error.size()>0)
        {   request.setAttribute("user",u);
            request.setAttribute("error" , error );
            request.getRequestDispatcher("addUser.jsp").forward(request , response) ;
        }
        else {
            userService.insertUser(u);
            request.setAttribute("MyUsers" , userService.findAll());
            request.getRequestDispatcher("home.jsp").forward(request, response);

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
