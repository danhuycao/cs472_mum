package cs472.mum.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cs472.mum.dao.UserDao;
import cs472.mum.model.User;
import cs472.mum.service.LoginService;
import cs472.mum.service.SignUpService;
//Didi Oumar
/**
 * Servlet implementation class SignUpServlet
 */
@WebServlet("/signup")
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");


        SignUpService sus = new SignUpService();


        List<String> signupErrorMessages = sus.validateSignUpForm(firstName, lastName, email, password);

        if (signupErrorMessages.size() > 0) {
            request.setAttribute("signupErrorMessages", signupErrorMessages);
            request.setAttribute("firstName", firstName);
            request.setAttribute("firstLast", lastName);
            request.setAttribute("email", email);


            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else {
            User user = new User();
            user.setFname(firstName);
            user.setLname(lastName);

            user.setEmail(email);

            user.setPassword(password);


            LoginService loginService = new LoginService();
            user.setPassword(user.getPassword());

            long id = new UserDao().insert(user);

            if (id > 0) {
                User newUser = (User) new UserDao().findById(id);
                List<User> users = (List<User>) new UserDao().findAll();
                HttpSession session = request.getSession();
                session.setAttribute("currentuser", newUser);
                session.setAttribute("MyUsers", users);

                if (newUser != null) {
                    response.sendRedirect("/ScheduledEventsServlet");
                }
            } else
                request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

}
