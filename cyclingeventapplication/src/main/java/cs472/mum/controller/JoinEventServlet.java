package cs472.mum.controller;

import cs472.mum.model.User;
import cs472.mum.service.CyclingEventService;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 *
 * @version1.0
 * @author Amr Ibrahim
 *
 */


@WebServlet("/JoinEventServlet")
public class JoinEventServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        CyclingEventService ds= new CyclingEventService();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String eventId = request.getParameter("event_id");
        System.out.println("eventId: " + eventId);

        User user = (User) request.getSession().getAttribute("currentuser");

        JSONObject[] results = ds.getJSONJoinEvent(eventId,user.getUserId());
        System.out.println(Arrays.toString(results));
        out.print(Arrays.toString(results));
        out.flush();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
