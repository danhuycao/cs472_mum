package cs472.mum.service;

import cs472.mum.dao.*;
import cs472.mum.enums.EventStatus;
import cs472.mum.model.*;
import org.json.simple.JSONObject;


import java.sql.SQLException;
import java.util.List;

/**
 * @author Amr Ibrahim, Dan Cao
 * @version1.0
 */


public class CyclingEventService {

    private CyclingEventDao cyclingEventDAO;

    public CyclingEventService() {
        cyclingEventDAO = new CyclingEventDao();
    }

    public CyclingEvent getCyclingEventById(int eventId) {
        return cyclingEventDAO.getCyclingEventById(eventId);
    }

    public List<CyclingEvent> getCyclingEventByUserId(long userId) {
        return cyclingEventDAO.getCyclingEventByUserId(userId);
    }

    public List<CyclingEvent> getCyclingEventByStatus(String status) {
        System.out.println("Entring getCyclingEventByStatus Service method");
        return cyclingEventDAO.getCyclingEventByStatus(status);
    }

    public JSONObject[] getJSONCyclingEventByStatus(String status) {
        System.out.println("Entring getJSONCyclingEventByStatus Service method");
        List<CyclingEvent> data = cyclingEventDAO.getCyclingEventByStatus(status);

        if (data.size() > 0) {
            JSONObject[] results = new JSONObject[data.size()];
            for (int i = 0; i < data.size(); i++) {
                JSONObject res = new JSONObject();
                res.put("id", data.get(i).getId());
                res.put("name", data.get(i).getName());
                res.put("route", data.get(i).getRoute());
                res.put("start_date", data.get(i).getStart_date().toString());
                res.put("status", data.get(i).getStatus());
                res.put("owner_id", data.get(i).getOwner_id());
                res.put("emergency_flag", data.get(i).getEmergency_flag());
                results[i] = res;
            }
            return results;
        }
        JSONObject[] objectToReturn = new JSONObject[1];
        JSONObject res = new JSONObject();
        res.put("error", "No results found");
        objectToReturn[0] = res;
        return objectToReturn;
    }


    public JSONObject[] getJSONEventParticipant(String eventId) {

        CyclingEventDao ced = new CyclingEventDao();
        System.out.println("Entring getJSONEventParticipant Service method");
        List<User> data = ced.getJSONEventParticipant(eventId);

        if (data.size() > 0) {
            JSONObject[] results = new JSONObject[data.size()];
            for (int i = 0; i < data.size(); i++) {
                JSONObject res = new JSONObject();
                res.put("userId", data.get(i).getUserId());
                res.put("fname", data.get(i).getFname());
                res.put("lname", data.get(i).getLname());
                res.put("email", data.get(i).getEmail());
                res.put("password", data.get(i).getPassword());

                results[i] = res;
            }
            return results;
        }
        JSONObject[] objectToReturn = new JSONObject[1];
        JSONObject res = new JSONObject();
        res.put("error", "No results found");
        objectToReturn[0] = res;
        return objectToReturn;

    }


    public JSONObject[] getJSONJoinEvent(String eventId, long userId) {

        CyclingEventDao ced = new CyclingEventDao();
        System.out.println("Entring getJSONJoinEvent Service method");
        Boolean joined = ced.getJSONJoinEvent(eventId, userId);

        JSONObject[] objectToReturn = new JSONObject[1];
        JSONObject res = new JSONObject();
        if (joined) {
            res.put("success", "true");
        } else {
            res.put("success", "false");
        }

        objectToReturn[0] = res;
        return objectToReturn;
    }

    public boolean insertCyclingEvent(CyclingEvent ce) throws SQLException {
        if (ce == null) throw new IllegalArgumentException("Cannot save null");
        boolean result = cyclingEventDAO.insertCyclingEvent(ce);

        insertEmrgencyForCyclingEvent(ce);

        return result;
    }

    public boolean updateCyclingEvent(CyclingEvent ce) throws SQLException {
        if (ce == null) throw new IllegalArgumentException("Cannot update null");

        boolean result = cyclingEventDAO.updateCyclingEvent(ce);
        ;

        insertEmrgencyForCyclingEvent(ce);

        return result;
    }

    private void insertEmrgencyForCyclingEvent(CyclingEvent ce) throws SQLException {
        if (ce.getStatus().equalsIgnoreCase(EventStatus.EMERGENCY.toString())) {
            cyclingEventDAO.insertEventEmergency(ce.getId(), ce.getEmergency_flag());
        } else {
            EmergencyEventService es = new EmergencyEventService();
            EmergencyEvent ee = new EmergencyEvent();
            ee.setEvent_id(ce.getId());
            es.deleteEmrEvent(ee);
        }
    }
}
