// Did Oumar
package cs472.mum.service;

import cs472.mum.dao.UserDao;
import cs472.mum.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserService {
    private UserDao userdao = new UserDao() ;
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public long insertUser(User u )
    {
        return userdao.insert(u);
    }

    public List<User> findAll( )
    {
        return userdao.findAll();
    }

    public List<String> validateUser(User u) {

        List<String> result = new ArrayList<String>();

        if (u.getFname().isEmpty()) {
            result.add("First name required");
        }
        if (u.getLname().isEmpty()) {
            result.add("Last name required");
        }


        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(u.getEmail());
        if (u.getEmail().isEmpty()|| !matcher.find()) {
            result.add("email should have Email format");
        }
        if (u.getPassword().length() <4 ) {
            result.add("password must be more than 3 characters)");
        }


        return result;
    }

}
