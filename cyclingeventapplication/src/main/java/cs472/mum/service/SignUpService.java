//// Didi
package cs472.mum.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//Didi
public class SignUpService {
	
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	
	
	public List<String> validateSignUpForm(String firstName, String lastName, String email,String password) {

		List<String> result = new ArrayList<String>();

		if (firstName.isEmpty()) {
			result.add("First name required");
		}
		if (lastName.isEmpty()) {
			result.add("Last name required");
		}


		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		if (email.isEmpty()|| !matcher.find()) {
			result.add("E-mail required");
		}
		if (password.isEmpty() ) {
			result.add("Last name required. (password must be more than 6 characters)");
		}


		return result;
	}

}
