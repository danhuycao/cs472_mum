package cs472.mum.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import cs472.mum.model.*;


/**
 * Didi Oumar
 * 

 *
 */
public class UserDao extends Dao implements IDao {

  private final String DB_TABLE = "User";

  public UserDao() {
    super();
  }

  /**
   * Implicit RowMapper Class
   * 
   *
   *
   */
  class UserDaoRowMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs) throws SQLException {
      User user = new User();
      user.setUserId(rs.getLong("user_id"));
      user.setFname(rs.getString("first_name"));
      user.setLname(rs.getString("last_name"));

      user.setEmail(rs.getString("email"));

      user.setPassword(rs.getString("password"));



      long userId = rs.getLong("user_id");

      return user;
    }

  }


  @Override
  public long insert(Model model) {

    User user = (User) model;

    String sql = String.format("INSERT INTO %s(first_name,last_name, password, email)"
        + "VALUE(?,?,?,?)", this.DB_TABLE);
    String[] returnId = {"user_id"};

    long id = 0;
    try {
      PreparedStatement statement = this.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      statement.setString(1, user.getFname());
      statement.setString(2, user.getLname());
      statement.setString(3, user.getPassword());
      statement.setString(4, user.getEmail());


      int st = statement.executeUpdate();
      if (st > 0) {
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
          id = rs.getInt(1);
        }
        rs.close();
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }finally{
      try{
        this.getConnection().close();
    }catch(Exception e){
      e.printStackTrace();
    }
  }
    return id;

  }

  @Override
  public boolean update(Model model) {
    User user = (User) model;
    String sql = String.format("UPDATE %s SET fname=?,lname=?, email=?, password=?,"
        + "phone_number=?, address=?, user_type=?, created_at=?,updated_at=?,picture=? "
        + "WHERE user_id=?", this.DB_TABLE);
    try {
      PreparedStatement statement = this.getConnection().prepareStatement(sql);
      statement.setString(1, user.getFname());
      statement.setString(2, user.getLname());
      statement.setString(4, user.getEmail());

      statement.setString(3, user.getPassword());



      statement.setLong(12, user.getUserId());
      if (statement.executeUpdate() > 0)
        return true;

    } catch (SQLException e) {
      e.printStackTrace();
    }finally{
      try{
        this.getConnection().close();
    }catch(Exception e){
      e.printStackTrace();
    }
  }
    return false;
  }

  @Override
  public boolean delete(Model model) {
    User user = (User) model;
    String sql = String.format("DELETE FROM %s WHERE userId=?", this.DB_TABLE);
    try {
      PreparedStatement statement = this.getConnection().prepareStatement(sql);
      statement.setLong(1, user.getUserId());
      if (statement.executeUpdate() > 0)
        return true;

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  @Override
  public Model findById(long Id) {
    String sql = String.format("SELECT * FROM %s WHERE user_id = ?", this.DB_TABLE);
    User user = null;;
    try {
      PreparedStatement statement = this.getConnection().prepareStatement(sql);
      statement.setLong(1, Id);
      ResultSet rs = statement.executeQuery();
      if (!rs.next())
        return null;
      user = new UserDaoRowMapper().mapRow(rs);
    } catch (SQLException e) {
      e.printStackTrace();
    }finally{
      try{
        this.getConnection().close();
    }catch(Exception e){
      e.printStackTrace();
    }
  }
    return user;
  }

  @Override
  public List<User> findAll() {
    String sql = String.format("SELECT * FROM %s", this.DB_TABLE);
    List<User> users = new ArrayList<>();
    try {
      ResultSet rs = this.query(sql);
      while (rs.next()) {
        users.add(new UserDaoRowMapper().mapRow(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }finally{
      try{
        this.getConnection().close();
    }catch(Exception e){
      e.printStackTrace();
    }
  }
    return users;
  }
  
  /**
	 * @author Didi Oumar
	 * @param email
	 * @param password
	 * @return
	 */
	
  public Model checkUser(String email , String password) {

		String sql = String.format("SELECT * FROM %s WHERE  email= ? and password =? ", this.DB_TABLE);
		User user = null;
		try {
			System.out.println("7777777777777");
			PreparedStatement statement = this.getConnection().prepareStatement(sql);
			statement.setString(1, email);
			statement.setString(2, password);
			
			ResultSet rs = statement.executeQuery();
			if (!rs.next()) return null;
			user = new UserDaoRowMapper().mapRow(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
          try{
            this.getConnection().close();
        }catch(Exception e){
          e.printStackTrace();
        }
      }
		return user;
	}
}
