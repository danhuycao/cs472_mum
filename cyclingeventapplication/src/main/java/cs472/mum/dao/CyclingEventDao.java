package cs472.mum.dao;

import cs472.mum.model.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @version1.0
 * @author Amr Ibrahim, Dan Cao
 *
 */


public class CyclingEventDao extends Dao {

    public CyclingEventDao() {
        super();
    }


    public List<CyclingEvent> getCyclingEventByStatus(String status){

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<CyclingEvent> events = new ArrayList<CyclingEvent>();
        try {
            String selectSQL = "SELECT * from CyclingEvent ce WHERE ce.status = ?";
            pstmt = this.getConnection().prepareStatement(selectSQL);
            pstmt.setString(1, status);
            rs = pstmt.executeQuery();
            while(rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String route = rs.getString("route");
                Date date = rs.getDate("start_date");
                String stts = rs.getString("status");
                int userId = rs.getInt("owner_id");
                String emergency_flag = rs.getString("emergency_flag");

                CyclingEvent ce = new CyclingEvent(id, name, route, date.toLocalDate(), stts, userId, emergency_flag);
                events.add(ce);
            }
            closeResultSet(rs);
        } catch(SQLException sqlEx) {
            printSQLException(sqlEx);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
            } catch (SQLException sqle) {
                printSQLException(sqle);
            }
        }
        return events;
    }

    public List<User> getJSONEventParticipant(String eventId){

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<User> users = new ArrayList<User>();
        try {
            String selectSQL = "SELECT u.* " +
                    "FROM EventParticipant ep, User u " +
                    "where " +
                    "ep.user_id = u.user_id and " +
                    "ep.event_id = ?";
            pstmt = this.getConnection().prepareStatement(selectSQL);
            pstmt.setString(1, eventId);
            rs = pstmt.executeQuery();
            while(rs.next()) {
                int user_id = rs.getInt("user_id");
                String first_name = rs.getString("first_name");
                String last_name = rs.getString("last_name");
                String password = rs.getString("password");
                String email = rs.getString("email");

                User u = new User();
                u.setUserId(user_id);
                u.setFname(first_name);
                u.setLname(last_name);
                u.setPassword(password);
                u.setEmail(email);

                users.add(u);
            }
            closeResultSet(rs);
        } catch(SQLException sqlEx) {
            printSQLException(sqlEx);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
            } catch (SQLException sqle) {
                printSQLException(sqle);
            }
        }
        return users;
    }

    public Boolean getJSONJoinEvent(String eventId, Long userId) {

        PreparedStatement pstmt = null;
        try {
            pstmt = this.getConnection().prepareStatement("insert into EventParticipant (event_id, user_id) values (?, ?)");
            pstmt.setInt(1, Integer.parseInt(eventId));
            pstmt.setInt(2, userId.intValue());

            pstmt.executeUpdate();
            return true;
        } catch(SQLException sqlEx) {
            printSQLException(sqlEx);
        } finally {
            // release resources
            try {
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
            } catch (SQLException sqle) {
                printSQLException(sqle);
            }
        }
        return false;
    }


    public static void printSQLException(SQLException e)    {
        while (e != null)      {
            System.err.println("\n----- SQLException -----");
            System.err.println("  SQL State:  " + e.getSQLState());
            System.err.println("  Error Code: " + e.getErrorCode());
            System.err.println("  Message:    " + e.getMessage());
            // for stack traces, refer to derby.log or uncomment this:
            //e.printStackTrace(System.err);
            e = e.getNextException();
        }
    }
    private void closeResultSet(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
                rs = null;
            }
        } catch (SQLException sqle) {
            printSQLException(sqle);
        }
    }
    public List<CyclingEvent> getCyclingEventByUserId(long userId){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<CyclingEvent> events = new ArrayList<CyclingEvent>();
        try {
            String selectSQL = "SELECT * from CyclingEvent ce WHERE ce.owner_id = ? order by start_date desc";
            pstmt = this.getConnection().prepareStatement(selectSQL);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while(rs.next()) {
                CyclingEvent ce = getCyclingEventFromResultSet(rs);
                events.add(ce);
            }
            closeResultSet(rs);
        } catch(SQLException sqlEx) {
            printSQLException(sqlEx);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
            } catch (SQLException sqle) {
                printSQLException(sqle);
            }
        }
        return events;
    }
    public CyclingEvent getCyclingEventById(int eventId){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String selectSQL = "SELECT * from CyclingEvent ce WHERE ce.id = ?";
            pstmt = this.getConnection().prepareStatement(selectSQL);
            pstmt.setInt(1, eventId);
            rs = pstmt.executeQuery();
            while(rs.next()) {
                CyclingEvent ce = getCyclingEventFromResultSet(rs);
                return ce;
            }
            closeResultSet(rs);
        } catch(SQLException sqlEx) {
            printSQLException(sqlEx);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
            } catch (SQLException sqle) {
                printSQLException(sqle);
            }
        }
        return null;
    }

    public boolean insertCyclingEvent(CyclingEvent ce) throws SQLException {

        String sql = "INSERT INTO CyclingEvent " +
                "(name,route,start_date,status,owner_id,emergency_flag) " +
                " VALUES(?, ?, ?, ?, ?, ?)";

        PreparedStatement statement = this.getConnection().prepareStatement(sql);
        statement.setString(1, ce.getName());
        statement.setString(2, ce.getRoute());
        Date date = Date.valueOf(ce.getStart_date());
        statement.setDate(3, date);
        statement.setString(4, ce.getStatus());
        statement.setLong(5, ce.getOwner_id());
        statement.setString(6, ce.getEmergency_flag());

        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowInserted;
    }

    public boolean insertEventEmergency(long eventId, String desc) throws SQLException {

        String sql = "INSERT INTO EventEmergency " +
                "(event_id,description) " +
                " VALUES(?, ?)";

        PreparedStatement statement = this.getConnection().prepareStatement(sql);
        statement.setLong(1, eventId);
        statement.setString(2, desc);

        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowInserted;
    }

    public boolean updateCyclingEvent(CyclingEvent ce) throws SQLException {

        String sql = "UPDATE CyclingEvent Set name=?, route=?, start_date=?, status=?, owner_id=?, emergency_flag=?" +
                "WHERE id=?";

        PreparedStatement statement = this.getConnection().prepareStatement(sql);
        statement.setString(1, ce.getName());
        statement.setString(2, ce.getRoute());
        Date date = Date.valueOf(ce.getStart_date());
        statement.setDate(3, date);
        statement.setString(4, ce.getStatus());
        statement.setLong(5, ce.getOwner_id());
        statement.setString(6, ce.getEmergency_flag());
        statement.setInt(7, ce.getId());

        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowInserted;
    }

    private CyclingEvent getCyclingEventFromResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String route = rs.getString("route");
        Date date = rs.getDate("start_date");
        String stts = rs.getString("status");
        int userId = rs.getInt("owner_id");
        String emergency_flag = rs.getString("emergency_flag");

        return new CyclingEvent(id, name, route, date.toLocalDate(), stts, userId, emergency_flag);
    }

}
