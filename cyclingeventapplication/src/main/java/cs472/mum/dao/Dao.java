// Didi
package cs472.mum.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This call is an abstract Data Acces Object Class to be extended by the IDao Interface.
 * All logic to make Database connections and run queries are handle by this classs
 * @author : Didi Oumar
 *
 */

public abstract class Dao {
       // change Here code
	private String DBHost = "localhost";
	private String DBPort = "3306";
	private String DBName = "cyclingclub";
	private String DBUsername = "root";
	private String DBPassword = "11111";

    /*private String DBHost = "cs472-backup.chua2ygtucub.us-east-2.rds.amazonaws.com";
    private String DBPort = "3306";
    private String DBName = "cyclingclub";
    private String DBUsername = "root";
    private String DBPassword = "root1234";*/

    /*private String DBHost = "cs472project.chua2ygtucub.us-east-2.rds.amazonaws.com";
    private String DBPort = "3306";
    private String DBName = "cyclingclub";
    private String DBUsername = "dancao";
    private String DBPassword = "Password1";*/

    /*private String DBHost = "cs472-backup-2.chua2ygtucub.us-east-2.rds.amazonaws.com";
    private String DBPort = "3306";
    private String DBName = "cyclingclub";
    private String DBUsername = "root";
    private String DBPassword = "root1234";*/
	
	private Connection connection;
	private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    /**
     * Constructor initialize and create the Connection object
     */
    public Dao() {
        String urlstring = "jdbc:mysql://"+this.DBHost+":"+this.DBPort+"/"+this.DBName+"?useSSL=true";

        try {
            Class.forName(DRIVER_NAME);
            try {
                this.connection = DriverManager.getConnection(urlstring, this.DBUsername, this.DBPassword);

            } catch (SQLException ex) {
                //exception:
                System.out.println("Failed to create the database connection.");
            }
        } catch (ClassNotFoundException ex) {
            //exception
            System.out.println("Database Driver not found.");
        }
    }
    /**
     * this method runs all valid SQL Statements and return a {@link ResultSet}
     * @param sql
     * @return
     */
    protected ResultSet query(String sql){
        ResultSet rs = null;
        try{
            Statement stmt = getConnection().createStatement();
            rs = stmt.executeQuery(sql);

        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return rs;
    }

    protected Connection getConnection() throws SQLException {
        String urlstring = "jdbc:mysql://"+this.DBHost+":"+this.DBPort+"/"+this.DBName+"?useSSL=false";
        if (this.connection == null || this.connection.isClosed()) {
            try {
                this.connection = DriverManager.getConnection(urlstring, this.DBUsername, this.DBPassword);
            } catch (SQLException ex) {
                //exception:

                System.out.println("Failed to create the database connection.");
                return null ;
            }
        }

        return this.connection;
    }

    protected void closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    protected void disconnect() throws SQLException {
        if (this.connection != null && !this.connection.isClosed()) {
            this.connection.close();
        }
    }

}
