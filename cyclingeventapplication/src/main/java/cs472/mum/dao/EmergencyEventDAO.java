package cs472.mum.dao;

import cs472.mum.model.CyclingEvent;
import cs472.mum.model.EmergencyEvent;
import cs472.mum.model.Model;
import cs472.mum.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmergencyEventDAO extends Dao implements IDao{

   private String DB_TABLE="EventEmergency";
        public EmergencyEventDAO() {
            super();
        }

        /**
         * Implicit RowMapper Class
         *
         *
         *
         */

        @Override
        public List<EmergencyEvent> findAll() {
            String sql = String.format("SELECT * FROM %s", this.DB_TABLE);
            List<EmergencyEvent> emregencies = new ArrayList<>();
            try {
                ResultSet rs = this.query(sql);
                while (rs.next()) {
                    emregencies.add(new EmergencyEventDAO.EmergencyEventDaoRowMapper().mapRow(rs));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }finally{
                try{
                    this.getConnection().close();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            return emregencies;
        }



    class EmergencyEventDaoRowMapper implements RowMapper<EmergencyEvent> {
            @Override
            public EmergencyEvent mapRow(ResultSet rs) throws SQLException {
                EmergencyEvent emrgency = new EmergencyEvent();
                emrgency.setEmergency_id(rs.getInt("emergency_id"));
                emrgency.setEvent_id(rs.getInt("event_id"));
                emrgency.setDescription(rs.getString("description"));
                emrgency.setStatus(rs.getString("status"));

                int userId = rs.getInt("emergency_id");

                return emrgency;
            }

        }

    @Override
    public long insert(Model model) {

        EmergencyEvent emergency = (EmergencyEvent) model;

        String sql = String.format("INSERT INTO %s(event_id,description, status)"
                + "VALUE(?,?,?)", this.DB_TABLE);
        String[] returnId = {"emergency_id"};

        long id = 0;
        try {
            PreparedStatement statement = this.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, emergency.getEvent_id());
            statement.setString(2, emergency.getDescription());
            statement.setString(3, emergency.getStatus());

            int st = statement.executeUpdate();
            if (st > 0) {
                ResultSet rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    id = rs.getInt(1);
                }
                rs.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            try{
                this.getConnection().close();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return id;
    }

    @Override
    public boolean update(Model model) {
        return false;
    }

    @Override
    public boolean delete(Model model) {
        EmergencyEvent emergency = (EmergencyEvent) model;
        String sql = "Delete FROM EventEmergency WHERE event_id=?";

        PreparedStatement statement = null;
        try {
            statement = this.getConnection().prepareStatement(sql);
            statement.setLong(1, emergency.getEvent_id());
            boolean rowInserted = statement.executeUpdate() > 0;
            statement.close();
            disconnect();
            return rowInserted;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public Model findById(long Id) {
        return null;
    }
}
