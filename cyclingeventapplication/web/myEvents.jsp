<%--
  Created by IntelliJ IDEA.
  User: dancao
  Date: 4/22/2018
  Time: 7:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>My Events - Cycling Club</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">

    <jsp:include page="/NavControl.jsp" flush="true"/>

    <table class="table table-bordered" id="MyEventTbl">
        <caption><h3>Events
            <a href="/EventServlet?a=addEvent" style="float: right;">Add Event</a></h3></caption>
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Start Date</th>
            <th>Status</th>
            <th>Emergency Description</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="ce" items="${MyEvents}">
            <tr>
                <td><c:out value="${ce.id}"/></td>
                <td><c:out value="${ce.name}"/></td>
                <td><c:out value="${ce.start_date_string}"/></td>
                <td><c:out value="${ce.status}"/></td>
                <td><c:out value="${ce.emergency_flag}"/></td>
                <td>
                    <a href="/EventServlet?a=updateEvent&id=<c:out value='${ce.id}' />">Edit</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<%--<script src="js/myevents.js"></script>--%>
</body>
</html>
