<%--
  Created by IntelliJ IDEA.
  User: dancao
  Date: 4/22/2018
  Time: 10:49 PM
  To change this template use File | Settings | File Templates.
--%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script src="js/Emergencyevent.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/tableEmergency.css">

<div id="divResult" class="alert alert-danger fade in hide" style="z-index: 100000;"></div>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Cycling Club</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="/ScheduledEventsServlet">Home</a></li>
                <%--<li><a href="/EventServlet">My Events</a></li>--%>
                <%--<li><a href="/ScheduledEventsServlet">Scheduled Events</a></li>--%>
                <li><a href="displayActiveEvents.jsp">Active Events</a></li>
                <li><a href="/home.jsp">View Users</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">My Account<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                       
                        <li><a href="/EventServlet">My Events</a></li>
                        <li><a href="/"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                    </ul>
                </li>
            </ul>
            <%--<ul class="nav navbar-nav navbar-right">
                &lt;%&ndash;<li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>&ndash;%&gt;
                <li><a href="/"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
            </ul>--%>
        </div>
    </div>
</nav>
