<%--
  Created by IntelliJ IDEA.
  User: dancao
  Date: 4/22/2018
  Time: 7:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Add Event - Cycling Club</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <jsp:include page="/NavControl.jsp" flush="true"/>

    <div class="alert alert-success" id="divStatusMsg">
        <span id="spanStatusMessage"></span>
    </div>

    <c:if test="${cyclingevent != null}">
    <form action="update" method="post"></c:if>
        <c:if test="${cyclingevent == null}">
        <form action="insert" method="post"></c:if>

            <div class="form-group">
                <h2>
                    <c:if test="${cyclingevent != null}">
                        Edit Event
                    </c:if>
                    <c:if test="${cyclingevent == null}">
                        Add New Event
                    </c:if>
                </h2>
            </div>

            <c:if test="${cyclingevent != null}">
                <input type="hidden" name="eventId" id="eventId" value="<c:out value='${cyclingevent.id}' />"/>
            </c:if>

            <div class="form-group">
                <label for="eventname">Name:</label>
                <input type="text" class="form-control" id="eventname" name="eventname"
                       value="<c:out value='${cyclingevent.name}' />">
            </div>
            <div class="form-group">
                <label for="start_date">Start Date:</label>
                <%--<input class="datepicker form-control" data-date-format="mm/dd/yyyy" id="start_date" name="start_date"
                       value="<c:out value='${cyclingevent.start_date}' />">--%>
                <input type="text" class="form-control" id="start_date" name="start_date"
                       value="<c:out value='${cyclingevent.start_date_string}' />">
            </div>
            <div class="form-group">
                <label>Route:</label>
                <input type="text" class="form-control" id="route" name="route"
                       value="<c:out value='${cyclingevent.route}' />">
            </div>

            <c:if test="${cyclingevent != null}">
                <jsp:scriptlet>
                                String[] eventStatus = new String[]{"Scheduled", "Active", "Pause", "Emergency"};
                                pageContext.setAttribute("eventStatus", eventStatus);
                </jsp:scriptlet>
            </c:if>
            <c:if test="${cyclingevent == null}">
                <jsp:scriptlet>
                                String[] eventStatus = new String[]{"Scheduled", "Active", "Pause"};
                                pageContext.setAttribute("eventStatus", eventStatus);
                </jsp:scriptlet>
            </c:if>

            <div class="form-group">
                <label>Status:</label>
                <select class="form-control" data-style="btn-info" id="selectStatus" name="selectStatus">
                    <c:forEach var="status" items="${eventStatus}">
                        <c:if test="${cyclingevent.status.equalsIgnoreCase(status)}">
                            <option class="form-control" selected><c:out value="${status}"/></option>
                        </c:if>
                        <c:if test="${!cyclingevent.status.equalsIgnoreCase(status)}">
                            <option class="form-control"><c:out value="${status}"/></option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>

            <div class="form-group" id="divDesc">
                <label for="eventname">Please provide the reason for Emergency status:</label>
                <input type="text" class="form-control" id="emergency_flag" name="emergency_flag"
                       value="<c:out value='${cyclingevent.emergency_flag}' />">
            </div>

            <div class="form-group">
                <button type="button" class="btn btn-primary" id="btnSaveEvent">Submit</button>
            </div>
        </form>

</div>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/myevents.js"></script>
</body>
</html>
