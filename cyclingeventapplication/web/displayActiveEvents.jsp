<%--
  Created by IntelliJ IDEA.
  User: Amr Ibrahim
  Date: 4/22/2018
  Time: 3:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">


    <jsp:include page="/NavControl.jsp" flush="true"/>

    <div class="alert" id="divStatusMsg">
        <span id="spanStatusMessage"></span>
    </div>

        <!--
        <button type="submit">Submit</button>
    -->

    <div id="result"></div>

    <div id="participants"></div>
</div>

    <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script  src="js/active.js"></script>

</body>
</html>
