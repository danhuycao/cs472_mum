// created by Amr Ibrahim
$(document).ready(function(){
    //$("button").click(function(event){
    //    event.preventDefault();

        $.get("/ActiveEventsServlet").done(processData).fail(ajaxFailure);

    //});

    $(document).on( "click", ".participants", function(){
        let eventId=$(this).attr("data-eventId");
        $.get("/EventParticipantServlet",{event_id:eventId})
            .done(processParticipantsData  ).fail(ajaxFailure);
    });

    $(document).on( "click", ".join", function(){
        let eventId=$(this).attr("data-eventId");
        $.post("/JoinEventServlet",{event_id:eventId})
            .done(processJoinEventData ).fail(ajaxFailure);
    });
    $("#divStatusMsg").hide();

});

function processJoinEventData(data) {
    console.log(data);
    $("#participants").empty();

    let color="red";
    let msg="unable to join";
    if(data[0].success === "true"){
        color="darkgreen";
        msg="success join";
        $("#divStatusMsg").addClass('alert-success').removeClass('alert-danger');
    }else{
        $("#divStatusMsg").addClass('alert-danger').removeClass('alert-success');
    }

    $("#divStatusMsg").show();//.css("");
    $("#divStatusMsg").html(msg);

    //$("<p style='color:" + color + ";'>" +data[0].success+ " </p>").appendTo("#participants");
}

function processParticipantsData(data){
    console.log(data);
    //populateComments(data,mypostId);
    $("#divStatusMsg").html("");
    $("#divStatusMsg").hide();

    $("#participants").empty();

    if(data[0].error){
        let errHTML = "<p style='color:red;'>" +data[0].error +"</p>";
        $(errHTML).appendTo("#participants");
        return;
    }

    let inHTML = "<table class=\"table table-bordered\">\n" +
        "    <tr>\n" +
        "        <th>ID</th>\n" +
        "        <th>First Name</th>\n" +
        "        <th>Last Name</th>\n" +
        "        <th>Email</th>\n" +
        "    </tr>";

    $.each(data, function(index, value){
        var newItem =
            "<tr> " +
            "<td>"  + value.userId + "</td>" +
            "<td>"  + value.fname + "</td>" +
            "<td>"  + value.lname + "</td>" +
            "<td>"  + value.email + "</td>" +
            "</tr>";
        inHTML += newItem;
    });

    inHTML += "</table>";

    $(inHTML).appendTo("#participants");

}

function ajaxFailure(xhr, status, exception) {
    console.log(xhr, status, exception);
}

function processData(data){

    console.log(data);

    $("#result").empty();


    let inHTML = "<table class=\"table table-bordered\">\n" +
        "    <tr>\n" +
        "        <th>ID</th>\n" +
        "        <th>Name</th>\n" +
        "        <th>Route</th>\n" +
        "        <th>Start Date</th>\n" +
        "        <th>Status</th>\n" +
        "        <th>Owner ID</th>\n" +
        "        <th>Emergency Flag</th>\n" +
        "        <th></th>\n" +
        "        <th></th>\n" +
        "\n" +
        "    </tr>";

    for(let i=0; i<data.length; i++){
        inHTML +=
            "<tr> " +
            "<td>" + data[i].id + " </td>"+
            "<td>" + data[i].name + " </td>"+
            "<td>" + data[i].route + " </td>"+
            "<td>" + data[i].start_date + " </td>"+
            "<td>" + data[i].status + " </td>"+
            "<td>" + data[i].owner_id + " </td>"+
            "<td>" + data[i].emergency_flag + " </td>"+
            "<td><input type='button' data-eventId="+data[i].id+" value='Show participants' class='participants' /></td>" +
            "<td><input type='button' data-eventId="+data[i].id+" value='Join' class='join' /></td>" +
            "</tr>";
    }
    inHTML += "</table>";

    $(inHTML).appendTo("#result");
}