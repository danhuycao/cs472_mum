<%--
  Created by IntelliJ IDEA.
  User: Didi
  Date: 4/22/2018
  Time: 7:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>User - Cycling Club</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">

    <jsp:include page="/NavControl.jsp" flush="true"/>

    <table class="table table-bordered" id="MyEventTbl">
        <caption><h3>List of User
            <a href="/addUser.jsp" style="float: right;">Add User</a></h3></caption>
        <thead>
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>email</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="us" items="${MyUsers}">
            <tr>
                <td><c:out value="${us.userId}"/></td>
                <td><c:out value="${us.fname}"/></td>
                <td><c:out value="${us.lname}"/></td>
                <td><c:out value="${us.email}"/></td>
                <%--<td>
                    <a href="/home.jsp=<c:out value='${us.userId}' />">Edit</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" data-id="<c:out value="${us.userId}"/>"
                       class="deleteMyEvent">Delete</a>
                </td>--%>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
