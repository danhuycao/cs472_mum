<%--
  Created by IntelliJ IDEA.
  User: Amr Ibrahim
  Date: 4/22/2018
  Time: 2:57 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Display Scheduled Events - Cycling Application</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

    <div class="container">

        <jsp:include page="/NavControl.jsp" flush="true"/>

        <div class="alert alert-success" id="divStatusMsg">
            <span id="spanStatusMessage"></span>
        </div>
        <table class="table table-bordered">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Route</th>
                <th>Start Date</th>
                <th>Status</th>
                <th>Owner ID</th>
                <th>Emergency Flag</th>

            </tr>
            <c:forEach var="event" items="${events}">
                <tr>
                    <td>${event.id}</td>
                    <td>${event.name}</td>
                    <td>${event.route}</td>
                    <td>${event.start_date}</td>
                    <td>${event.status}</td>
                    <td>${event.owner_id}</td>
                    <td>${event.emergency_flag}</td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/scheduledevents.js"></script>
</body>
</html>
