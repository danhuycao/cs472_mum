<%--
  Created by IntelliJ IDEA.
  User: dancao
  Date: 4/22/2018
  Time: 10:49 PM
  To change this template use File | Settings | File Templates.
--%>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Cycling Club</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="/ScheduledEventsServlet">Home</a></li>
                <%--<li><a href="/EventServlet">My Events</a></li>--%>
                <%--<li><a href="/ScheduledEventsServlet">Scheduled Events</a></li>--%>
                <li><a href="displayActiveEvents.jsp">Active Events</a></li>
                <li><a href="/home.jsp">View Users</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">My Account<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/EventServlet">My Events</a></li>
                        <li><a href="/"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                    </ul>
                </li>
            </ul>
            <%--<ul class="nav navbar-nav navbar-right">
                &lt;%&ndash;<li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>&ndash;%&gt;
                <li><a href="/"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
            </ul>--%>
        </div>
    </div>
</nav>
