$(function () {
    $("#btnSaveEvent").click(function () {
        var postData = {
            id: $("#eventId").val(),
            name: $("#eventname").val(),
            route: $("#route").val(),
            start_date: $("#start_date").val(),
            status: $("#selectStatus").val(),
            emergency_flag: $("#emergency_flag").val()
        };

        if($("#selectStatus").val() === "Emergency" && $("#emergency_flag").val() == ""){
            alert("Please provide the emrgency description.");
            return;
        }

        var action = "saveEvent";
        if($("#eventId").length == 1){
            action = "modifyEvent";
        }

        $.ajax({
            url: "/EventServlet?a=" + action,
            type: "post",
            data: postData,
            success: function (results) {
                showMessage();
                cleanForm();
            },
            error: function (xhr, code, exception) {
                ajaxFailure(xhr, code, exception);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    });

    $("#selectStatus").change(function(){
        var val = $(this).val();
        if(val == "Emergency"){
            $("#divDesc").show();
        }else{
            $("#divDesc").hide();
        }
    });

    hideMessage();
    $("#divDesc").hide();
});

function cleanForm() {
    if($("#eventId").length == 0){
        $("#eventname").val("");
        $("#route").val("");
        $("#start_date").val("");
        $("#selectStatus").val("");
        $("#emergency_flag").val("");
    }
}

function showMessage(msg) {
    $("#divStatusMsg").show();
    $("#divStatusMsg").html("Save successful!");
}

function hideMessage(msg) {
    $("#divStatusMsg").hide();
    $("#divStatusMsg").html("");
}

function ajaxFailure(xhr, status, exception) {
    console.log(xhr, status, exception);
}