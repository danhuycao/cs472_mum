CREATE DATABASE 'CyclingClub' /*!40100 DEFAULT CHARACTER SET latin1 */;

CREATE TABLE 'Route' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'name' varchar(45) NOT NULL,
  'From' varchar(45) NOT NULL,
  'To' varchar(45) NOT NULL,
  PRIMARY KEY ('id'),
  UNIQUE KEY 'id_UNIQUE' ('id'),
  UNIQUE KEY 'name_UNIQUE' ('name')
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE 'User' (
  'user_id' int(11) NOT NULL AUTO_INCREMENT,
  'user_name' varchar(25) NOT NULL,
  'password' varchar(50) NOT NULL,
  'email' varchar(45) DEFAULT NULL,
  'phone' varchar(12) DEFAULT NULL,
  'address' varchar(100) DEFAULT NULL,
  'city' varchar(45) DEFAULT NULL,
  'state' varchar(45) DEFAULT NULL,
  'zipcode' varchar(10) DEFAULT NULL,
  PRIMARY KEY ('user_id'),
  UNIQUE KEY 'user_id_UNIQUE' ('user_id'),
  UNIQUE KEY 'user_name_UNIQUE' ('user_name')
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE 'CyclingEvent' (
  'id' int(11) NOT NULL,
  'name' varchar(45) NOT NULL,
  'route_id' int(11) DEFAULT NULL,
  'start_date' datetime NOT NULL,
  'status' varchar(45) NOT NULL,
  'owner_id' int(11) DEFAULT NULL,
  'emergency' bit(1) DEFAULT b'0',
  PRIMARY KEY ('id'),
  UNIQUE KEY 'id_UNIQUE' ('id'),
  UNIQUE KEY 'name_UNIQUE' ('name'),
  KEY 'route_idx' ('route_id'),
  KEY 'event_owner_idx' ('owner_id'),
  CONSTRAINT 'event_owner' FOREIGN KEY ('owner_id') REFERENCES 'User' ('user_id') ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT 'route' FOREIGN KEY ('route_id') REFERENCES 'Route' ('id') ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE 'UserCycling' (
  'user_id' int(11) NOT NULL,
  'cycling_event_id' int(11) NOT NULL,
  PRIMARY KEY ('user_id','cycling_event_id')
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


